package datatypes;

public class DatosComentarioCompleto {

	private int id;
	private String texto;
	private String mail;
	private int likes;
	private int dislikes;
	
	public DatosComentarioCompleto(){}
	
	public DatosComentarioCompleto(int id, String texto, String mail, int likes, int dislikes){
		this.setId(id);
		this.setTexto(texto);
		this.setMail(mail);
		this.setLikes(likes);
		this.setDislikes(dislikes);
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public int getDislikes() {
		return dislikes;
	}

	public void setDislikes(int dislikes) {
		this.dislikes = dislikes;
	}

	@Override
	public String toString() {
		return "DatosComentarioCompleto [id=" + id + ", texto=" + texto + ", mail=" + mail + ", likes=" + likes
				+ ", dislikes=" + dislikes + "]";
	}

	
}
