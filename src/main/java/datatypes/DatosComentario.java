package datatypes;

public class DatosComentario {

	private int id;
	private String texto;
	private String mail;
	
	DatosComentario(){}
	
	public DatosComentario(int id, String texto){
		this.setId(id);
		this.setTexto(texto);
	}
	
	public DatosComentario(int id, String texto, String mail){
		this.setId(id);
		this.setTexto(texto);
		this.setMail(mail);
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "DatosComentario [id=" + id + ", texto=" + texto + "]";
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	
}
