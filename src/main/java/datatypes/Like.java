package datatypes;

public class Like {
	
	private int idComentario;
	private boolean likeComentario;
	
	
	public Like() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Like(int idComentario, boolean likeComentario) {
		super();
		this.idComentario = idComentario;
		this.likeComentario = likeComentario;
	}


	public int getIdComentario() {
		return idComentario;
	}


	public void setIdComentario(int idComentario) {
		this.idComentario = idComentario;
	}


	public boolean isLikeComentario() {
		return likeComentario;
	}


	public void setLikeComentario(boolean likeComentario) {
		this.likeComentario = likeComentario;
	}
	
	
}
