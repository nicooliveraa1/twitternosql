package datatypes;

public class Respuesta<T> {

  private String codigo;
  private String mensaje;
  private T datos;
    
    public Respuesta(){}
    
    public Respuesta(String codigo, String mensaje, T datos){
    this.codigo = codigo;
    this.mensaje = mensaje;
    this.datos = datos;
    }

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public T getDatos() {
		return datos;
	}

	public void setDatos(T datos) {
		this.datos = datos;
	}
    
    
}