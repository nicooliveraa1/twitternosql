package datatypes;

public class DatosUsuario {

	private String mail;
	
	public DatosUsuario(){}
	
	public DatosUsuario(String mail){
		this.mail = mail;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	
	
}
