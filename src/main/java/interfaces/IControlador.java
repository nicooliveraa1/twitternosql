package interfaces;

import java.util.List;

import datatypes.DatosComentario;
import datatypes.DatosComentarioCompleto;
import datatypes.DatosUsuario;
import datatypes.Respuesta;

public interface IControlador {
    
    public Respuesta<DatosUsuario> crearUsuario(String mail);
    
    public Respuesta<List<DatosComentario>> crearComentario(String mail, String texto);
    
    public Respuesta<List<DatosComentario>> listarComentariosUsuario (String mail);
    
    public Respuesta<DatosComentarioCompleto> reaccionarComentario(int idComentario, boolean like);
    
    public Respuesta<DatosComentarioCompleto> leerComentario(int idComentario);
    
}