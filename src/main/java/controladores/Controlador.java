package controladores;

import java.util.LinkedList;
import java.util.List;


import datatypes.DatosComentario;
import datatypes.DatosUsuario;
import datatypes.Respuesta;
import datatypes.DatosComentarioCompleto;
import interfaces.IControlador;
import mongoDBA.Conexion;

public class Controlador implements IControlador{

	public Controlador(){}
	
	//Se pasar� el correo electr�nico del usuario a crear,
	//este correo debe ser �nico en el sistema,
	public Respuesta<DatosUsuario> crearUsuario(String mail) {
		Respuesta respuesta;
		try {
			Conexion conexion = Conexion.getInstance();
			if(!mail.isEmpty() && !conexion.existeUsuario(mail)) {
				conexion.crearUsuario(mail);
				respuesta = new Respuesta("200", "Se ingreso correctamente", new DatosUsuario(mail));
			} else {
				respuesta = new Respuesta("402", mail.isEmpty() ? "Debe ingresar un mail" : "El usuario ya existe", null);
			}
		} catch (Exception e) {
			respuesta = new Respuesta("500", e.getMessage(), null);
		}
		return respuesta;
	}
	
	//Se pasar� el correo electr�nico de un usuario del sistema, y el texto
	//de los comentarios (menor a 256 caracteres).El sistema le asignar� un identificador deasda
	//forma autom�tica al comentario creado.
	public Respuesta<List<DatosComentario>> crearComentario(String mail, String texto) {
		Respuesta respuesta;
		try {
			Conexion conexion = Conexion.getInstance();
			if(conexion.existeUsuario(mail) == true) {
				if(texto.length() < 256 && !texto.isEmpty() && !mail.isEmpty()) {
					conexion.crearComentario(mail, texto);
					respuesta = new Respuesta("200", "Se ingreso correctamente", conexion.listarComentariosUsuario(mail));
				} else {
					respuesta = new Respuesta("402", mail.isEmpty() ? "Debe ingresar un mail" : "El texto es nulo o demasiado largo", null);
				}
			} else {
				respuesta = new Respuesta("402", "El usuario no existe", null);
			}
		} catch (Exception e) {
			respuesta = new Respuesta("500", e.getMessage(), null);
		}
		return respuesta;
	}

	//Se pasar� el correo electr�nico de un usuario del
	//sistema, y se retorna una lista con el identificador y contenido de todos los comentarios
	//realizados por el usuario, junto con su identificador.
	public Respuesta<List<DatosComentario>> listarComentariosUsuario (String mail){
		Respuesta respuesta;
		try {
			List<DatosComentario> comentarios = new LinkedList<DatosComentario>();
				Conexion conexion = Conexion.getInstance();
				if(!mail.isEmpty() && conexion.existeUsuario(mail)) {
					comentarios = conexion.listarComentariosUsuario(mail);
					respuesta = new Respuesta("200", "Se ingreso correctamente", comentarios);
				} else {
					respuesta = new Respuesta("402", "El usuario no existe o no fue ingresado", null);
				}
		} catch (Exception e) {
			respuesta = new Respuesta("500", e.getMessage(), null);
		}
		return respuesta;
	}
	
	//Agregar emoci�n al comentario - Se pasar� el identificador de un comentario existente
	//en el sistema, y un calificador de emoci�n hacia el comentario que puede ser: Me gusta, No
	//me gusta. Se debe verificar que el usuario exista en el sistema.
	public Respuesta<DatosComentarioCompleto> reaccionarComentario(int idComentario, boolean like) {
		Respuesta respuesta;
		try {
			Conexion conexion = Conexion.getInstance();
			conexion.reaccionarComentario(idComentario, like);
			respuesta = new Respuesta("200", "Se ingreso correctamente", conexion.leerComentario(idComentario));
		} catch (Exception e) {
			respuesta = new Respuesta("500", "El comentario ingresado no existe", null);
		}
		return respuesta;
	}
	
	//Se pasar� el identificador de un comentario existente en el sistema, y
	//se retorna el identificador del comentario, el texto que lo contiene, identificador de usuario
	//de qui�n lo cre�, y cantidad de me gustas y no me gusta del comentario.
	public Respuesta<DatosComentarioCompleto> leerComentario(int idComentario) {
        Respuesta respuesta;
        try {
            DatosComentarioCompleto comentario = null;
            Conexion conexion = Conexion.getInstance();
            comentario = conexion.leerComentario(idComentario);
            if (comentario != null) {
                respuesta = new Respuesta("200", "Exito", comentario);
            } else {
                respuesta = new Respuesta("404", "El comentario no existe", null);
            }
        } catch (Exception e) {
            respuesta = new Respuesta("500", e.getMessage(), null);
        }
        return respuesta;
    }
}
