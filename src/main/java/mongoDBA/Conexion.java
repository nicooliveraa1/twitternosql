package mongoDBA;

import java.util.LinkedList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

import datatypes.DatosComentario;
import datatypes.DatosComentarioCompleto;

public class Conexion {
	
	private static Conexion instancia = null;
	
	private static MongoDatabase database;
	private static MongoCollection<Document> usuarios;
	private static MongoCollection<Document> comentarios;
	
	public static Conexion getInstance() 
    { 
        if (instancia == null) { 
        	instancia = new Conexion();
        }
        udpate();
        return instancia; 
    } 
	
	Conexion(){
		udpate();
	}
	
	
	private static void udpate() {
		MongoClientURI uri = new MongoClientURI("mongodb+srv://admin:admin@cluster0.jdjwt.mongodb.net/twitter?retryWrites=true&w=majority");
		MongoClient mongoClient = new MongoClient(uri);
		database = mongoClient.getDatabase("twitter");
		usuarios = database.getCollection("usuarios");
		comentarios = database.getCollection("comentarios");
	}
	
	
	public void crearUsuario(String mail) {
		Document usuario = new Document("mail",mail);
		usuarios.insertOne(usuario);
		udpate();
	}
	
	public boolean existeUsuario(String mail) {
		udpate();
		System.out.print(mail);
		FindIterable<Document> iterable = database.getCollection("usuarios").find(new Document("mail", mail));
		if(iterable.first() == null) {
			return false;
		}else {
			return true;
		}
		
	}

	public void crearComentario(String mail, String texto) {
		udpate();
		int elementos = (int) comentarios.countDocuments();
		Document comentario = new Document("id",elementos+1).append("mail", mail).append("texto", texto).append("likes", 0).append("dislikes", 0);
		comentarios.insertOne(comentario);
		udpate();
		
	}

	
	public List<DatosComentario> listarComentariosUsuario(String mail) {
		List<DatosComentario> dtComentarios = new LinkedList<DatosComentario>();
		udpate();
		FindIterable<Document> comentariosDocuments = database.getCollection("comentarios").find(new Document("mail", mail));
		for(Document cD : comentariosDocuments) {
			DatosComentario dtc = new DatosComentario((int)cD.get("id"),(String)cD.get("texto"), (String)cD.get("mail"));
			dtComentarios.add(dtc);
		}
		return dtComentarios;
	}

	
	
	public void reaccionarComentario(int idComentario, boolean like) {
		udpate();
		FindIterable<Document> comentariosDocuments = database.getCollection("comentarios").find(new Document("id", idComentario));
		DatosComentarioCompleto dtc = null;
		for(Document cD : comentariosDocuments) {
			dtc = new DatosComentarioCompleto((int)cD.get("id"),(String)cD.get("texto"), (String)cD.get("mail"), (int)cD.get("likes"), (int)cD.get("dislikes"));
		}
		int dislikes = dtc.getDislikes();
		int likes = dtc.getLikes();
		if(like == true) {
			likes++;
			comentarios.updateOne(Filters.eq("id", idComentario), new Document("$set", new Document("likes", likes)));
		}else {
			dislikes++;
			comentarios.updateOne(Filters.eq("id", idComentario), new Document("$set", new Document("dislikes", dislikes)));
		}
		udpate();
	}
	

	public DatosComentarioCompleto leerComentario(int idComentario) {
		udpate();
		FindIterable<Document> comentariosDocuments = database.getCollection("comentarios").find(new Document("id", idComentario));
		DatosComentarioCompleto dtc = null;
		for(Document cD : comentariosDocuments) {
			dtc = new DatosComentarioCompleto((int)cD.get("id"),(String)cD.get("texto"), (String)cD.get("mail"), (int)cD.get("likes"), (int)cD.get("dislikes"));
		}	
		return dtc;
		
	}
	
	

}
