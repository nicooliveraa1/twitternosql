package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import controladores.Controlador;
import datatypes.DatosComentario;
import datatypes.DatosComentarioCompleto;
import datatypes.DatosUsuario;
import datatypes.Like;
import datatypes.Respuesta;
import interfaces.Fabrica;
import interfaces.IControlador;


//Indiciamos que es un controlador rest
@RestController
@RequestMapping("/api") //esta sera la raiz de la url, es decir http://127.0.0.1:8080/api/
public class RestControlador {

	//@Autowired
	private Fabrica fab = Fabrica.getInstancia();
	private IControlador ic = fab.getControlador();

	@PostMapping("/users")
	public Respuesta<DatosUsuario> crearUsuario(@RequestBody DatosUsuario user){
		return ic.crearUsuario(user.getMail());
	}
	
	@PostMapping("/comentarios")
	public Respuesta<List<DatosComentario>> crearComentario(@RequestBody DatosComentario comentario ){
		return ic.crearComentario(comentario.getMail(),comentario.getTexto());
	}
	
	@GetMapping("/comentarios/{id}")
	public Respuesta<DatosComentarioCompleto> verComentario(@PathVariable int id){
		return ic.leerComentario(id);
	}
	
	@GetMapping("/comentarios/")
	public Respuesta<List<DatosComentario>> verComentariosDeUsuario(@RequestBody String mail){
		return ic.listarComentariosUsuario(mail);
	}
	
	@PutMapping("/comentarios/")
	public Respuesta<DatosComentarioCompleto> reaccionarComentario(@RequestBody Like likearComentario ){
		int idComentario = likearComentario.getIdComentario();
		boolean reaccionComentario = likearComentario.isLikeComentario();
		return ic.reaccionarComentario(idComentario, reaccionComentario);
	}
}
